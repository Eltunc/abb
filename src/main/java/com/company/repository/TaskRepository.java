package com.company.repository;

import com.company.model.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query(value = "INSERT INTO tasks_and_users(task_id,user_id) VALUES(:taskId,:assignedUserId)", nativeQuery = true)
    @Modifying
    @Transactional
    void assignTaskToUser(Integer taskId, Integer assignedUserId);

    @Query(value = "SELECT t.* " +
            "FROM tasks t " +
            "WHERE t.id IN (SELECT tau.task_id " +
            "FROM tasks_and_users tau " +
            "WHERE tau.user_id = :userId)", nativeQuery = true)
    List<Task> getAllTaskByUser(Integer userId);

    @Query(value = "select * from tasks where id=:taskId",nativeQuery = true)
    Task getTaskById(Integer taskId);

}
