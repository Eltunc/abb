package com.company.repository;

import com.company.model.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrganizationRepository extends JpaRepository<Organization,Long> {

    @Query(value = "SELECT * "+
            "FROM organizations o " +
            "LEFT JOIN users u on u.organization_id = o.id " +
            "WHERE u.id = :userId", nativeQuery = true)
    Organization findByUserId(Integer userId);
}
