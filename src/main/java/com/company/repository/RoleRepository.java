package com.company.repository;

import com.company.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query(value = "UPDATE roles SET role_name=:roleName WHERE id=:roleId", nativeQuery = true)
    @Modifying
    @Transactional
    void updateRole(Long roleId, String roleName);

    @Query(value = "SELECT * from roles where id in :roleIdList",nativeQuery = true)
    List<Role> findRoleById(List<Integer> roleIdList);

    @Query(value = "select * from roles where role_name=:roleName", nativeQuery = true)
    Role getAdminRole(String roleName);
}
