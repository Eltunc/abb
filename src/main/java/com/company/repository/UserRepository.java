package com.company.repository;

import com.company.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByEmailAndPassword(String email, String password);

    @Query(value = "SELECT u.* " +
            "FROM users u " +
            "WHERE u.id IN (SELECT tau.user_id " +
            "FROM tasks_and_users tau " +
            "WHERE tau.task_id = :taskId)",nativeQuery = true)
    List<User> getAllUserByTask(Integer taskId);

    @Query(value = "select email from users where id=:id", nativeQuery = true)
    String findEmailById(Integer id);

    @Query(value = "select * from users where email=:email",nativeQuery = true)
    User findByEmail(String email);

}
