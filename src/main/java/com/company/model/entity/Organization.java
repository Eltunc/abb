package com.company.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "organizations")
public class Organization {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String phone;
    private String address;

}
