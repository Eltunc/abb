package com.company.model.entity;

import com.company.model.enums.Status;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name="tasks")
public class Task {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Please enter Title to Task")
    private String title;

    @NotBlank(message = "Please enter Description to Task")
    private String description;

    @NotNull(message = "Please enter DeadLine to Task")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deadLine;

    @NotNull(message = "Please enter status to Task")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Override
    public String toString() {
        return String.format("Problemin başlığı : " + title + ".\n" +
                "Problem haqqında ümumi məlumat : " + description + ".\n" +
                "Problemin həlli üçün verilən sonuncu vaxt: " + deadLine);
    }
}
