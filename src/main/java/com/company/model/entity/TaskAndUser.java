package com.company.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="tasks_and_users")
public class TaskAndUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "task_id")
    private Task task;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
