package com.company.model.dto;

import lombok.Data;

@Data
public class OrganizationDto {

    private Long id;
    private String name;
    private String phone;
    private String address;

}
