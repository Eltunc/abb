package com.company.model.dto;

import lombok.Data;

@Data
public class SignUpDto {
    OrganizationDto organizationDto;
    UserDto userDto;
}
