package com.company.model.dto;

import com.company.model.enums.Status;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskDto {

    private Long id;

    private String title;

    private String description;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deadLine;

    private Status status;

    public TaskDto(String title, String description, LocalDateTime deadLine, Status status) {
        this.title = title;
        this.description = description;
        this.deadLine = deadLine;
        this.status = status;
    }

    @Override
    public String toString() {
        return String.format("Problemin başlığı : " + title + ".\n" +
                "Problem haqqında ümumi məlumat : " + description + ".\n" +
                "Problemin həlli üçün verilən sonuncu vaxt: " + deadLine);
    }
}
