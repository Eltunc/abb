package com.company.model.enums;

public enum Status {

    NEW,
    INCOMPLETE,
    COMPLETE
}
