package com.company.configurations;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class UserSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, "/organizations/sign-up").permitAll()
                .antMatchers(HttpMethod.POST, "/users/sign-in").permitAll()
                .antMatchers(HttpMethod.GET, "/roles/by/admin").permitAll()
                .antMatchers(HttpMethod.POST, "/roles/update").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/roles/{roleName}").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/roles/{roleId}").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/tasks").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/tasks/assignment").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/tasks/all").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.POST, "/users/sign-up").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and().httpBasic()
                .and().csrf().disable();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Bean
    public PasswordEncoder noOpPasswordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

}
