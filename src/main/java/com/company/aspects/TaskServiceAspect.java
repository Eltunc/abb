package com.company.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
@Slf4j
public class TaskServiceAspect {

    @Before("execution(* com.company.serivce.TaskService.*(..))")
    public void beforeAllTaskServicesMethod(JoinPoint joinPoint) {
        log.info("method location {}", joinPoint.getSignature());
        log.info("method parameters {}", Arrays.toString(joinPoint.getArgs()));
    }

    @AfterReturning(value = "execution(* com.company.serivce.TaskService.*(..))", returning = "result")
    public void afterReturningAllTaskServicesMethod(Object result) {
        log.info("result {}", result);
    }

    @AfterThrowing(value = "execution(* com.company.serivce.TaskService.*(..))", throwing = "exception")
    public void afterExceptionAllTaskServicesMethod(Throwable exception) {
        log.error("error {}", exception);
    }

}
