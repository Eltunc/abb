package com.company.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
@Slf4j
public class UserServiceAspect {

    @Before("execution(* com.company.serivce.UserService.*(..))")
    public void beforeAllUserServicesMethod(JoinPoint joinPoint) {
        log.info("method location {}", joinPoint.getSignature());
        log.info("method parameters {}", Arrays.toString(joinPoint.getArgs()));
    }

    @AfterReturning(value = "execution(* com.company.serivce.UserService.*(..))", returning = "result")
    public void afterReturningAllUserServicesMethod(Object result) {
        log.info("result is {}", result);
    }

    @AfterThrowing(value = "execution(* com.company.serivce.UserService.*(..))", throwing = "exception")
    public void afterExceptionAllUserServicesMethod(Throwable exception) {
        log.error("error is {}", exception);
    }
}
