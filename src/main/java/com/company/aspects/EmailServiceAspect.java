package com.company.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
@Slf4j
public class EmailServiceAspect {

    @Before("execution(* com.company.serivce.EmailService.*(..))")
    public void beforeAllEmailServicesMethod(JoinPoint joinPoint) {
        log.info("method location {}", joinPoint.getSignature());
        log.info("method parameters {}", Arrays.toString(joinPoint.getArgs()));
    }

    @AfterReturning(value = "execution(* com.company.serivce.EmailService.*(..))", returning = "result")
    public void afterReturningAllEmailServicesMethod(Object result) {
        log.info("result is {}", result);
    }

    @AfterThrowing(value = "execution(* com.company.serivce.EmailService.*(..))", throwing = "exception")
    public void afterExceptionAllEmailServicesMethod(Throwable exception) {
        log.error("error is {}", exception);
    }
}
