package com.company.serivce;

import com.company.model.dto.RoleDto;

public interface RoleService {

    RoleDto createRole(String roleName);

    void updateRole(RoleDto roleDto);

    void deleteRole(Long roleId);

    RoleDto getAdminRole();
}
