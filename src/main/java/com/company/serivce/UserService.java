package com.company.serivce;

import com.company.model.dto.UserDto;
import com.company.model.entity.Organization;
import com.company.model.entity.User;

import java.util.List;

public interface UserService {

    void registrationAdmin(Organization organization, UserDto userDto);

    void registrationUser(Integer adminId, UserDto userDto);

    UserDto loginUser(UserDto userDto);

    List<UserDto> getAllUserByTask(Integer taskId);

    String findEmailById(Integer id);

    User findByEmail(String email);

}
