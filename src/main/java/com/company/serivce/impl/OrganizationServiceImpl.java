package com.company.serivce.impl;

import com.company.model.dto.SignUpDto;
import com.company.model.entity.Organization;
import com.company.repository.OrganizationRepository;
import com.company.serivce.OrganizationService;
import com.company.serivce.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrganizationServiceImpl implements OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final UserService userService;
    private final ModelMapper modelMapper=new ModelMapper();

    @Override
    public void registrationAdminAndOrganization(SignUpDto signUpDto) {
        Organization organization=modelMapper.map(signUpDto.getOrganizationDto(), Organization.class);
        Organization savedOrganization = organizationRepository.save(organization);
        userService.registrationAdmin(savedOrganization,signUpDto.getUserDto());
    }
}
