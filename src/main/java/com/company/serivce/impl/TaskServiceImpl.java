package com.company.serivce.impl;

import com.company.model.dto.TaskDto;
import com.company.model.entity.Task;
import com.company.repository.TaskRepository;
import com.company.serivce.EmailService;
import com.company.serivce.TaskService;
import com.company.serivce.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    private final UserService userService;
    private final EmailService emailService;

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public void createTask(TaskDto taskDto) {
        Task task = modelMapper.map(taskDto, Task.class);
        taskRepository.save(task);
    }

    @Override
    public List<Task> getAllTask() {
        return taskRepository.findAll();
    }

    @Override
    public void assignTaskToUser(Integer adminId, Integer taskId, Integer assignedUserId) {
        taskRepository.assignTaskToUser(taskId, assignedUserId);
        String adminEmail=userService.findEmailById(adminId);
        String userEmail=userService.findEmailById(assignedUserId);
        Task task=getTaskById(taskId);
        emailService.sender(adminEmail,userEmail,task.getTitle(), task.toString());
    }

    @Override
    public List<Task> getAllTaskByUser(Integer userId) {
        return taskRepository.getAllTaskByUser(userId);
    }

    @Override
    public Task getTaskById(Integer taskId) {
        return taskRepository.getTaskById(taskId);
    }
}
