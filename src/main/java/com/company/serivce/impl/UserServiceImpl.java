package com.company.serivce.impl;

import com.company.exception.ResourceNotFoundException;
import com.company.model.dto.UserDto;
import com.company.model.entity.Organization;
import com.company.model.entity.Role;
import com.company.model.entity.User;
import com.company.repository.OrganizationRepository;
import com.company.repository.RoleRepository;
import com.company.repository.UserRepository;
import com.company.serivce.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final OrganizationRepository organizationRepository;

    private final RoleRepository roleRepository;
    private final ModelMapper modelMapper = new ModelMapper();


    private void registration(Organization organization, List<Role> roleList, UserDto userDto) {
        User user = modelMapper.map(userDto, User.class);
        user.setOrganization(organization);
        user.setRoleList(roleList);
        userRepository.save(user);
    }

    @Override
    public void registrationAdmin(Organization organization, UserDto userDto) {
        List<Role> roleList = roleRepository.findRoleById(userDto.getRoleIdList());
        registration(organization, roleList, userDto);

    }

    @Override
    public void registrationUser(Integer adminId, UserDto userDto) {
        Organization organization = organizationRepository.findByUserId(adminId);
        if (organization == null) {
            throw new ResourceNotFoundException("No Admin belonging to such an organization was found");
        }
        List<Role> roleList = roleRepository.findRoleById(userDto.getRoleIdList());
        registration(organization, roleList, userDto);
    }

    @Override
    public UserDto loginUser(UserDto userDto) {
        User user = userRepository.findUserByEmailAndPassword(userDto.getEmail(), userDto.getPassword());
        return modelMapper.map(user, UserDto.class);
    }

    @Override
    public List<UserDto> getAllUserByTask(Integer taskId) {
        List<User> userList = userRepository.getAllUserByTask(taskId);
        return userList.stream().map(user -> modelMapper.map(user, UserDto.class)).collect(Collectors.toList());
    }

    @Override
    public String findEmailById(Integer id) {
        return userRepository.findEmailById(id);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

}
