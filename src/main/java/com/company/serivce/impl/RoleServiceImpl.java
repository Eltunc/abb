package com.company.serivce.impl;

import com.company.model.dto.RoleDto;
import com.company.model.entity.Role;
import com.company.repository.RoleRepository;
import com.company.serivce.RoleService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final ModelMapper modelMapper=new ModelMapper();

    @Override
    public RoleDto createRole(String roleName) {
        Role role = new Role();
        role.setRoleName("ROLE_"+roleName);
        return modelMapper.map(roleRepository.save(role),RoleDto.class);
    }

    @Override
    public void updateRole(RoleDto roleDto) {
        roleRepository.updateRole(roleDto.getId(), roleDto.getRoleName());
    }

    @Override
    public void deleteRole(Long roleId) {
        roleRepository.deleteById(roleId);
    }

    @Override
    public RoleDto getAdminRole() {

        Role role=roleRepository.getAdminRole("ROLE_ADMIN");
        if (role==null){
            return createRole("ADMIN");
        }else {
            return modelMapper.map(role,RoleDto.class);
        }
    }
}
