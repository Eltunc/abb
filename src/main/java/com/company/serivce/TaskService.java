package com.company.serivce;

import com.company.model.dto.TaskDto;
import com.company.model.entity.Task;

import java.util.List;

public interface TaskService {

    void createTask(TaskDto taskDto);

    List<Task> getAllTask();

    void assignTaskToUser(Integer adminId, Integer taskId, Integer assignedUserId);

    List<Task> getAllTaskByUser(Integer userId);

    Task getTaskById(Integer taskId);
}
