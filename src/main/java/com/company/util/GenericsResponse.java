package com.company.util;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
public class GenericsResponse {

    private String message;

    private List<String> errors = new ArrayList<>();


    public GenericsResponse(String message, List<String> errors) {
        this.message = message;
        this.errors = errors;
    }

    private GenericsResponse(String message, String error){
        this.message=message;
        this.errors= List.of(error);
    }
}
