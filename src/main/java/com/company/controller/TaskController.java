package com.company.controller;

import com.company.model.dto.TaskDto;
import com.company.model.entity.Task;
import com.company.serivce.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("tasks")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @PostMapping
    public void createTask(@RequestBody TaskDto taskDto) {
        taskService.createTask(taskDto);
    }

    @GetMapping("all")
    public ResponseEntity<List<Task>> getAllTask() {
        return ResponseEntity.ok(taskService.getAllTask());
    }

    @GetMapping("assignment")
    public void assignTaskToUser(@RequestHeader("loggedInAdmin") Integer adminId,
                                 @RequestParam("taskId") Integer taskId,
                                 @RequestParam("assignedUserId") Integer assignedUserId) {
        taskService.assignTaskToUser(adminId, taskId, assignedUserId);
    }

    @GetMapping("by-user")
    public ResponseEntity<List<Task>> getAllTaskByUser(@RequestParam("userId") Integer userId) {
        return ResponseEntity.ok(taskService.getAllTaskByUser(userId));
    }

}
