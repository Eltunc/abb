package com.company.controller;

import com.company.model.dto.RoleDto;
import com.company.serivce.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("roles")
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleService;

    @GetMapping("{roleName}")
    public void createRole(@PathVariable("roleName") String roleName) {
        roleService.createRole(roleName);
    }

    @PostMapping("update")
    public void updateRole(@RequestBody RoleDto roleDto) {
        roleService.updateRole(roleDto);
    }

    @DeleteMapping("{roleId}")
    public void deleteRole(@PathVariable Long roleId){
        roleService.deleteRole(roleId);
    }


    @GetMapping("by/admin")
    public ResponseEntity<RoleDto> getAdminRole(){
        return ResponseEntity.ok(roleService.getAdminRole());
    }

}
