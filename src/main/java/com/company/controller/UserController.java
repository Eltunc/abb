package com.company.controller;

import com.company.model.dto.UserDto;
import com.company.serivce.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("sign-up")
    public void registrationUser(@RequestHeader(value = "loggedInAdmin") Integer adminId, @RequestBody UserDto userDto){
        userService.registrationUser(adminId,userDto);
    }

    @PostMapping("sign-in")
    public ResponseEntity<UserDto> loginUser(@RequestBody UserDto userDto) {
        return ResponseEntity.ok(userService.loginUser(userDto));
    }

    @GetMapping("by-task")
    public ResponseEntity<List<UserDto>> getAllTaskByUser(@RequestParam("taskId") Integer taskId){
        return ResponseEntity.ok(userService.getAllUserByTask(taskId));
    }

}
