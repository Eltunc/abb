package com.company.controller;

import com.company.model.dto.SignUpDto;
import com.company.serivce.OrganizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("organizations")
@RequiredArgsConstructor
public class OrganizationController {
    private final OrganizationService organizationService;

    @PostMapping("sign-up")
    public void registrationAdminAndOrganization(@RequestBody SignUpDto signUpDto) {
        organizationService.registrationAdminAndOrganization(signUpDto);
    }
}
