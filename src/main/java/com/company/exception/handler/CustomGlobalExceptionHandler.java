package com.company.exception.handler;

import com.company.util.GenericsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.NonUniqueResultException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Iterator;
import java.util.List;

@RestControllerAdvice
@Slf4j
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleIncorrectDataException(ConstraintViolationException exception) {
        log.error("Incorrect Data Entered {}",exception);
        Iterator<ConstraintViolation<?>> constraintViolationIterator = exception.getConstraintViolations().iterator();
        GenericsResponse genericsResponse = new GenericsResponse();
        genericsResponse.setMessage("Incorrect Data Entered");
        genericsResponse.setErrors(List.of(constraintViolationIterator.next().getMessage()));
        return new ResponseEntity<>(genericsResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NonUniqueResultException.class)
    public ResponseEntity<Object> handleLoginTimeIncorrectResultException(NonUniqueResultException exception) {
        log.error("An Error Occurred During Login {}",exception);
        GenericsResponse genericsResponse = new GenericsResponse();
        genericsResponse.setMessage("You cannot log in to this account for security reasons");
        genericsResponse.setErrors(List.of(exception.getMessage()));
        return new ResponseEntity<>(genericsResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleResourceNotFoundException(IllegalArgumentException exception) {
        log.error("Resource Not Found Exception {}",exception);
        GenericsResponse genericsResponse = new GenericsResponse();
        genericsResponse.setMessage("Email or Password incorrect");
        genericsResponse.setErrors(List.of(exception.getMessage()));
        return new ResponseEntity<>(genericsResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public ResponseEntity<Object> handleUpdateException(SQLIntegrityConstraintViolationException exception) {
        log.error("Update Exception {}",exception);
        GenericsResponse genericsResponse = new GenericsResponse();
        genericsResponse.setMessage("There is no such user or task");
        genericsResponse.setErrors(List.of(exception.getMessage()));
        return new ResponseEntity<>(genericsResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

//    @ExceptionHandler({SMTPAddressFailedException.class, MailSendException.class})
//    public ResponseEntity<Object> handleMailException(SMTPAddressFailedException smtpAddressFailedException, MailSendException exception){
//        log.error("Incorrect Mail problem {}", smtpAddressFailedException);
//        GenericsResponse genericsResponse=new GenericsResponse();
//        genericsResponse.setMessage(smtpAddressFailedException.getMessage());
//        genericsResponse.setErrors(List.of(smtpAddressFailedException.getCommand()));
//        return new ResponseEntity<>(genericsResponse,HttpStatus.INTERNAL_SERVER_ERROR);
//    }

}
